<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Votesystem Controller
 *
 * @property \App\Model\Table\VotesystemTable $Votesystem
 *
 * @method \App\Model\Entity\Votesystem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VotesystemController extends AppController
{
    public function vote()
    {
        //voteアクション内で特に処理を行わないので記述なし
    }

    public function result()
    {
        date_default_timezone_set('Asia/Tokyo');
        //Postデータの取得
        $postData = $this->request->getData('id'); // vote.ctpから入力データとしてidを取得

        if($postData != null)
        {
            $voteSystemTable = $this->getTableLocator()->get('votesystem'); //votesystemテーブルの取得
            $targetRecord = $voteSystemTable->get($postData); //votesystemテーブルから該当IDのレコードを取得
            $targetRecord->Popularity = $targetRecord['Popularity'] + 1; //レコードのPopularityを更新
            $targetRecord->Date = date('Y-m-d H:i:s');
            $voteSystemTable->save($targetRecord);  //更新したレコードの保存
            
            //レコード件数取得
            $recordNum = $this->Votesystem->find()->count();

            //全レコードのPopularityを取得
            for($i=1; $i <= $recordNum; $i++)
            {
                $tmpRecord = $voteSystemTable->get($i); //Idが1から順番にレコードを取得
                $this->set('vote'.$i, $tmpRecord['Popularity']); //該当レコードのPopularityをViewに渡す
                $this->set('date'.$i, $tmpRecord['Date']);
            }

            $mostPopularity = $this->Votesystem->find()->select(['mostPopularity' => $this->Votesystem->find()->func()->max('Popularity')])->first(); //Popularityの最大値を取得　連想配列キーmostPopularityに値$this->Votesystem->find()->func()->max('Popularity')を代入
            $ret = $this->Votesystem->find()->select(['Name'])->where(['Popularity' => (int)$mostPopularity->mostPopularity])->toArray(); //Popularityの最大値と同じレコードを抽出して配列に格納

            //Popularityがトップの名前の表示用の文字列を用意
            $mostPopularityNames = "";
            for ($j=0; $j < count($ret); $j++)
            { 
                $mostPopularityNames = $mostPopularityNames.$ret[$j]['Name']."さん ";
            }

            $this->set('mostPopularity', $mostPopularityNames); //人気トップのNameをViewに渡す
        }
        else 
        {
            $clear = $this->request->getData('clear');
            if($clear == 1)
            {
                $voteSystemTable = $this->getTableLocator()->get('votesystem');
                $recordNum = $this->Votesystem->find()->count();
                for($i=1; $i <= $recordNum; $i++)
                {
                    $tmpRecord = $voteSystemTable->get($i); //Idが1から順番にレコードを取得
                    $tmpRecord->Popularity = 0;
                    $tmpRecord->Date = '';
                    $voteSystemTable->save($tmpRecord);  //更新したレコードの保存
                    $this->set('vote'.$i, $tmpRecord['Popularity']); //該当レコードのPopularityをViewに渡す
                    $this->set('date'.$i, $tmpRecord['Date']);
                }
                $mostPopularityNames = "もう一度投票が必要";
                $this->set('mostPopularity', $mostPopularityNames);
            }
        }
    }

    public function find()
    {
        $postName = $this->request->query('name');
        if($postName == '')
        {
            $this->set('name', '');
            $this->set('vote', ''); //該当レコードのPopularityをViewに渡す
            $this->set('date', '');
        }
        else 
        {
            $voteSystemTable = $this->getTableLocator()->get('votesystem');
            $recordNum = $this->Votesystem->find()->count();

            for ($i=1; $i <= $recordNum; $i++)
            {
                $targetRecord = $voteSystemTable->get($i);
                if($targetRecord->Name == $postName)
                {
                    $this->set('name', $targetRecord['Name']);
                    $this->set('vote', $targetRecord['Popularity']); //該当レコードのPopularityをViewに渡す
                    $this->set('date', $targetRecord['Date']);
                    break;
                }
                else 
                {
                    $this->set('name', '');
                    $this->set('vote', ''); //該当レコードのPopularityをViewに渡す
                    $this->set('date', '');
                }
            }
        }        
    }
}