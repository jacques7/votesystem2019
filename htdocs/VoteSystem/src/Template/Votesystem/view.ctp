<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Votesystem $votesystem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Votesystem'), ['action' => 'edit', $votesystem->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Votesystem'), ['action' => 'delete', $votesystem->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $votesystem->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Votesystem'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Votesystem'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="votesystem view large-9 medium-8 columns content">
    <h3><?= h($votesystem->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($votesystem->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($votesystem->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Popularity') ?></th>
            <td><?= $this->Number->format($votesystem->Popularity) ?></td>
        </tr>
    </table>
</div>
