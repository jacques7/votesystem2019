<?php
echo $this->Html->css('VotesystemController');?>

 <h2>結果発表</h2>
 <h3>Aさんの得票数は……</h3>
 <h4><?php echo $vote1; ?></h4>
  <p>です。</p>
  <p>最後の投票時間：<?php echo $date1 ?></p>

 <h3>Bさんの得票数は……</h3>
 <h4><?php echo $vote2; ?></h4>
  <p>です。</p>
  <p>最後の投票時間：<?php echo $date2 ?></p>

 <h3>Cさんの得票数は……</h3>
 <h4><?php echo $vote3; ?></h4>
  <p>です。</p>
  <p>最後の投票時間：<?php echo $date3 ?></p>

<h3>人気トップは……</h3>
<h4><?php echo $mostPopularity; ?></h4>
<p>です。</p>

<font size="5" color="15848F">
<b>
<?=$this->Form->create('VotesystemController', ['url' => ['action' => 'vote'], 'type' => 'post'])?>
 <?=$this->Form->submit('戻る')?>
 <?=$this->Form->end()?>
</b>
</font>

<font size="5" color="15848F"><b>
<?=$this->Form->create('VotesystemController', ['url' => ['action' => 'result'], 'type' => 'post'])?>
<?=$this->Form->hidden( "clear", [ "value" => 1 ] ) ?>
<?=$this->Form->submit('クリア')?>
<?=$this->Form->end()?>
</b></font>