<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Votesystem $votesystem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $votesystem->Id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $votesystem->Id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Votesystem'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="votesystem form large-9 medium-8 columns content">
    <?= $this->Form->create($votesystem) ?>
    <fieldset>
        <legend><?= __('Edit Votesystem') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Popularity');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
