<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VotesystemTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VotesystemTable Test Case
 */
class VotesystemTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\VotesystemTable
     */
    public $Votesystem;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Votesystem'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Votesystem') ? [] : ['className' => VotesystemTable::class];
        $this->Votesystem = TableRegistry::getTableLocator()->get('Votesystem', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Votesystem);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
